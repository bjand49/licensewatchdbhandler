INSERT INTO [dbo].[dashboardfeed](AttentionLevel, Category, EntityId, Description, Resolve1  , Resolve2  , Resolve3  , Resolve4  , Resolve5  , UpdateCondition, SystemUserID  , showable, UpdateTime )
VALUES
(1,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,GETDATE()),
(2,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,GETDATE()),
(3,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,GETDATE()),
(4,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,CURRENT_DATE()),
(5,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,GETDATE()),
(6,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,GETDATE()),
(7,3,12,'dummytext','dummy resolve text1','dummy resolve text2','dummy resolve text3','dummy resolve text4','dummy resolve text5',0,12,0,GETDATE());
INSERT INTO [dbo].[DashboardStatus] (EntityID, Title, [Update],UpdateTime,UpdateMinIntervalMinutes)
VALUES
(1,'Test title',2,GETDATE(),500),
(2,'Test title',2,GETDATE(),500),
(3,'Test title',2,GETDATE(),500),
(4,'Test title',2,GETDATE(),500),
(5,'Test title',2,GETDATE(),500),
(6,'Test title',2,GETDATE(),500);
INSERT INTO [dbo].[DashboardChart](ChartId,SystemUserID,EntityId,ChartDescription)
VALUES
(1,2,3,'text text og atter text'),
(2,2,3,'text text og atter text'),
(3,2,3,'text text og atter text'),
(4,2,3,'text text og atter text'),
(5,2,3,'text text og atter text'),
(6,2,3,'text text og atter text');