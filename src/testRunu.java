import Handlers.SQLHandler;
import Handlers.SQLWriter;
import InformationExperts.DataBaseHolder;
import InformationExperts.Row;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by B on 17-05-2016.
 */
public class testRunu
{
    public static void main(String[] args)
    {
        SQLHandler sqlHandler = new SQLHandler();
        DataBaseHolder startDB = sqlHandler.getTables("Databaser_Doc_Redigeret/Database_V1.SQL");
        DataBaseHolder endDB = sqlHandler.getTables("Databaser_Doc_Redigeret/Database_V2.SQL");

        try
        {
            startDB.getDataFromTables();
        }
        catch (Exception e)
        {
            startDB.getTableList().get(0).setRows(dummyData());
        }

        endDB.getData(startDB);
        SQLWriter.writeToFile(startDB.toSQLString());
    }
    public static List<Row> dummyData()
    {
        Row row = new Row("1","3","12","dummytext","dummy resolve text1","dummy resolve text2","dummy resolve text3","dummy resolve text4","dummy resolve text5","0","12","0","GETDATE()");
        Row row1 = new Row("2","3","12","dummytext","dummy resolve text1","dummy resolve text2","dummy resolve text3","dummy resolve text4","dummy resolve text5","0","12","0","GETDATE()");
        Row row2 = new Row("3","3","12","dummytext","dummy resolve text1","dummy resolve text2","dummy resolve text3","dummy resolve text4","dummy resolve text5","0","12","0","GETDATE()");
        ArrayList<Row> rows = new ArrayList<>();
        rows.add(row);
        rows.add(row1);
        rows.add(row2);
        return rows;
    }
}

