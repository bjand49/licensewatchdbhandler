package InformationExperts;

/**
 * Created by lordni on 5/12/16.
 */
public class Column {
    private String name;
    private String type;
    private boolean isUnique, isPrimary, isNullable;
    private int length, increment, startSeed, precision;

    public Column(String name, String type, boolean isNullable)
    {
        this.name = name;
        this.type = type;
        this.isNullable = isNullable;
    }

    public Column(String name, String type, boolean isNullable, int length)
    {
        this.name = name;
        this.type = type;
        this.isNullable = isNullable;
        this.length = length;
    }

    /***
     *
     * @return SQL Statement to make a table create
     */
    public String toSQLString() {
        String valueReturn = "\t[" + name + "]";
        if(type.equalsIgnoreCase("varchar")) {
            valueReturn += " [" + type + "] (" + length + ")";
        } else {
            valueReturn += " [" + type + "]";
        }
        if(!isNullable) {
            valueReturn += " NOT";
        }
        valueReturn += " NULL,\n";

        return valueReturn;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public boolean isUnique()
    {
        return isUnique;
    }

    public void setUnique(boolean unique)
    {
        isUnique = unique;
    }

    public boolean isPrimary()
    {
        return isPrimary;
    }

    public void setPrimary(boolean primary)
    {
        isPrimary = primary;
    }

    public boolean isNullable()
    {
        return isNullable;
    }

    public void setNullable(boolean nullable)
    {
        isNullable = nullable;
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int length)
    {
        this.length = length;
    }

    public int getIncrement()
    {
        return increment;
    }

    public void setIncrement(int increment)
    {
        this.increment = increment;
    }

    public int getStartSeed()
    {
        return startSeed;
    }

    public void setStartSeed(int startSeed)
    {
        this.startSeed = startSeed;
    }

    public int getPrecision()
    {
        return precision;
    }

    public void setPrecision(int precision)
    {
        this.precision = precision;
    }
}
