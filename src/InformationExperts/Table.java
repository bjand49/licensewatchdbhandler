package InformationExperts;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by lordni on 5/12/16.
 */
public class Table {
    private final String name;
    private List<Column> columns;
    private List<Row> rows;

    public Table(String name, List<Column> columns)
    {
        this.name = name;
        this.columns = columns;
        this.rows = new ArrayList<>();
    }

    public Table(String name, List<Column> columns, List<Row> rows)
    {
        this.name = name;
        this.columns = columns;
        this.rows = rows;
    }

    /*
    public Table(String name)
    {
        this.name = name;
    }
    */

    public List<Row> getRows()
    {
        return rows;
    }

    public void setRows(List<Row> rows)
    {
        this.rows = rows;
    }

    public String getName()
    {
        return name;
    }

    public List<Column> getColumns()
    {
        return columns;
    }

    public void setColumns(List<Column> columns)
    {
        this.columns = columns;
    }

    private void restructureColumns(Map<Integer,Integer> map, Table table)
    {
        Column[] arrCol = new Column[map.size()];
        for (Map.Entry<Integer,Integer> entry:map.entrySet())
        {
            if(entry.getValue() != null)
            {
                arrCol[entry.getValue()] =  columns.get(entry.getKey());
            }
            else if(entry.getValue() == null)
            {
                arrCol[entry.getKey()] = table.getColumns().get(entry.getKey());
                restructureRows(null);
            }
        }
        columns = Arrays.asList(arrCol);
        System.out.println(columns);
    }
    public void adaptTo(Table table)
    {
            Map<Integer,Integer> structureMap = getRestructureList(table);
            restructureColumns(structureMap,table);
    }
    private void restructureRows(String value)
    {
        for (Row row: rows)
        {
            row.addtoRow(value);
        }
    }
    private Map<Integer,Integer> getRestructureList(Table table)
    {
        Map<Integer,Column> availableCols = tableStructureToMap(this);
        Map<Integer,Column> nStructure = tableStructureToMap(table);
        Map<Integer,Integer> workingStucture = new HashMap<>();

        for (int i = 0; i < table.getColumns().size(); i++)
        {
            if(availableCols.get(i)== null)
            {
                workingStucture.put(i,null);
            }
            else if(nStructure.get(i).toSQLString().equals(availableCols.get(i).toSQLString()))
            {
                workingStucture.put(i,i);
                availableCols.remove(i);
            }
            else
            {
                workingStucture.put(i,null);
            }
        }
        Iterator<Map.Entry<Integer,Column>> itt = availableCols.entrySet().iterator();
        while (itt.hasNext())
        {
            Map.Entry<Integer,Column> entry = itt.next();
            boolean enter = false;
            for (Column col: nStructure.values())
            {
                if(col.toSQLString().equals(entry.getValue().toSQLString()))
                {
                    enter = true;
                    break;
                }
            }
            if(enter)
            {
                int i = -1;
                for (Integer j: nStructure.keySet() )
                {
                    if(nStructure.get(j).getName().equals(entry.getValue().getName()))
                    {
                        i = j;
                        break;
                    }
                }
                workingStucture.replace(entry.getKey(),i);
            }
        }

        return workingStucture;
    }
    private Map<Integer,Column> tableStructureToMap(Table table)
    {
        Map<Integer,Column> structure = new HashMap<>();

        for (int i = 0; i < table.getColumns().size(); i++)
        {
            structure.put(i,table.getColumns().get(i));
        }
        return structure;
    }
    public String toSQLString(String dbName)
    {
        String valueReturn = "CREATE TABLE [" + dbName + "].[" + name + "](\n";
        Column primaryCol = null;
        for(Column col: columns)
        {
            if(col.isPrimary())
            {
                primaryCol = col;
            }

            valueReturn += col.toSQLString();
        }
        if(primaryCol != null)
        {
            valueReturn += "\tCONSTRAINT [PK_" + name + "] PRIMARY KEY CLUSTERED (\n" +
                    "\t[" + primaryCol.getName() + "] ASC\t\n" + ")";
        }
        valueReturn += ")\n";
        if(rows.size() != 0)
        {
            valueReturn += "INSERT INTO " + name + "(";
            for (Column c : columns)
            {
                valueReturn += "[" + c.getName() + "], ";
            }
            valueReturn = valueReturn.substring(0, valueReturn.length() - 2);
            valueReturn += ")\n" +
                    "VALUES\n";
            for (Row r : rows)
            {
                valueReturn += "(" + r.toSQLString() + "),\n";
            }
            valueReturn = valueReturn.substring(0, valueReturn.length() - 2);
            valueReturn += ";";
        }
        return valueReturn;
    }

}
