package InformationExperts;

import Handlers.SQLConnector;
import Handlers.SQLHandler;

import java.util.List;

/**
 * Created by lordni on 5/12/16.
 */
public class DataBaseHolder {
    private  String name;
    private List<Table> tableList;
    private List<View> viewList;
    private List<StoredProcedures> SPList;

    public String toSQLString() {
        String valueReturn = "";
        for(Table table: tableList) {
            valueReturn += table.toSQLString(name) + "\n";
        }
        return valueReturn;
    }

    public void printTableDataDetails() {
        for(Table table: tableList) {
            System.out.println("TableName: " + table.getName());
            for(Row row: table.getRows()){
                System.out.println(row.toSQLString());
            }
        }
    }

    public void getDataFromTables() {
        for(Table table: tableList) {
            table.setRows(SQLConnector.getInstance().getRowDataFrom(table));
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTableList(List<Table> tableList)
    {
        this.tableList = tableList;
    }

    public List<Table> getTableList()
    {
        return tableList;
    }

    public void setViewList(List<View> viewList)
    {
        this.viewList = viewList;
    }

    public void setSPList(List<StoredProcedures> SPList)
    {
        this.SPList = SPList;
    }
    public void getData(DataBaseHolder startDb)
    {
        for (Table table : startDb.getTableList())
        {
            boolean tableFound = false;
            for (Table dataTable : tableList)
            {
                if(dataTable.getName().equals(table.getName()))
                {
                    tableFound = true;

                    table.adaptTo(dataTable);
                }
            }
            if(!tableFound)
            {
                System.out.println("Table not found: " + table.getName());
            }
        }
    }
}
