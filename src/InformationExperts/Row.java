package InformationExperts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** * Created by B on 17-05-2016.
 */

public class Row {
    List<String> rowData;

    public Row(String... data) {
        this.rowData = new ArrayList<>(Arrays.asList(data));
    }

    public String toSQLString() {
        String valueReturn = "";
        for (String str : rowData) {
            valueReturn += str + ", ";
        }
        valueReturn = valueReturn.substring(0,valueReturn.length()-2);
        return valueReturn;
    }

    public void addtoRow(String str)
    {
        rowData.add(str);
    }
}