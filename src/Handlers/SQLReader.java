package Handlers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

/**
 * Klassen der indlæser vores SQL fil. Den skal finde de forskellige create statements der er i
 * SQL filen. For hvert statement den finder klipper den det ud og gemmer det som en streng, der
 * bliver smidt ind i en liste. Når klassen er færdig med et læse returnerer den en liste
 * af de statements der er.
 */
public class SQLReader
{
    /***
     * Hovedmetoden i vores reader klasse.
     */
    public List<String> reader(String path)
    {
        List<String> savedSQLStatements = new LinkedList<>();

        try
        {
            //Læser database filen ind
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            while ((line = br.readLine()) != null)
            {
                if (line.contains("CREATE TABLE"))
                {
                    String sqlString = line;
                    int counter = 1;

                    while (true)
                    {
                        line = br.readLine();
                        sqlString += line;
                        if(line.contains("("))
                        {
                            if(counter == -1)
                            {
                                counter = 1;
                            }
                            else
                            {
                                counter++;
                            }
                        }
                        if(line.contains(")"))
                        {
                            counter--;
                        }
                        if(counter == 0)
                        {
                            break;
                        }
                    }
                    savedSQLStatements.add(sqlString);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return savedSQLStatements;
    }
}