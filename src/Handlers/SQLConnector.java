package Handlers;

import InformationExperts.Row;
import InformationExperts.Table;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristina on 24-05-2016.
 */
public class SQLConnector {
    private static SQLConnector connector;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    private SQLConnector(){
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1/dbo?useSSL=false", "root", "root");
            statement = connection.createStatement();
        }
        catch(SQLException eSQL)
        {
            System.out.println("SQL shit");
        }
        catch(ClassNotFoundException eCNF)
        {
            System.out.println("Class shit");
        }
    }

    public List<Row> getRowDataFrom(Table table)
    {
        int cols = table.getColumns().size();
        String[] dataArr = new String[cols];
        String tbName = table.getName();
        List<Row> valueReturn = new ArrayList<>();
        try
        {
            resultSet = statement.executeQuery("SELECT * FROM " + tbName + ";");
            while(resultSet.next())
            {
                for(int i = 1; i < cols; i++)
                {
                    dataArr[i] = resultSet.getString(i);
                }
                //System.out.println(a);
                valueReturn.add(new Row(dataArr));
            }
        }
        catch(SQLException eSQL)
        {
            System.out.println("ResultSet Bug");
        }
        return valueReturn;
    }

    public static SQLConnector getInstance() {
        if(connector == null)
            connector = new SQLConnector();
        return connector;
    }
}
