package Handlers;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * Created by B on 25-05-2016.
 */
public class SQLWriter
{
    public static void writeToFile(String sqlString)
    {
        PrintWriter writer;
        try
        {
            writer = new PrintWriter("sqlDump/SQLSCRIPT.SQL", "UTF-8");
            writer.println(sqlString);
            writer.close();

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        System.out.println("Write done");
    }
}
