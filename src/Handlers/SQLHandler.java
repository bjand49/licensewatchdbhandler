package Handlers;

import InformationExperts.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Denne klasse er lavet til at oprette tables, columns og rows som objekter, ud fra en tekst streng.
 * Vi får et helt create statement ind, og klipper i det, så vi kan få de relevante data ud
 */
public class SQLHandler {
    SQLReader reader = new SQLReader();
    /***
     * @param path
     * Fortæller hvor SQL filen vi skal læse fra er.
     * Constructoren bliver ikke brugt endnu, men med brug af computeren kan vi kalde
     * en handler der tager imod en sti på et SQL script
     */
    public SQLHandler(String path)
    {
        //Læs data fra en path ind i readerSQL
    }

    /***
     * Tom constructor der bliver brugt for at teste vores handler.
     */
    public SQLHandler()
    {

    }

    /***
     *
     * @param path
     * Stien til hvor vi skal læse vores dataen fra.
     *
     * @return
     * Metoden der returnerer listen af tables der er i databasen.
     * Den kalder en privat metode der tager et parameter af SQLReader for at simplificere det i computer klassen.
     *
     */
    public DataBaseHolder getTables(String path)
    {
        //få et array af strings fra readerSQL
        return createTable(path);

    }

    /***
     * En medtode der blev brugt til at sortere SET ANSI_NULLS og sådan noget fra, for at få fat i det mere
     * relevante kode(i forhold til vores scope). Den er nu ligegyldig da vi sorterer dem fra i SQLReader filen istedet.
     *
     * @param str
     * Strengen der indeholder det "ligegyldige kode", som vi skal fjerne.
     *
     * @return
     * Returnerer en ArrayListe med hver linje opdelt ved "\n".
     */
    private ArrayList<String> removeAnsiQuotedToArray(String str)
    {
        String[] temp = str.split("\n");
        ArrayList<String> words = new ArrayList<>(Arrays.asList(temp));
        for (int i = 0; i<words.size(); i++)
        {
            switch (words.get(i))
            {
                case "SET ANSI_NULLS ON":
                    words.remove(i);
                    i--;
                    break;
                case "GO":
                    try
                    {
                        String firstLine = words.get(i+1).split(" ")[0];
                        String secondLine = words.get(i+2).split(" ")[0];

                        if(
                            words.get(i+1).equals("SET QUOTED_IDENTIFIER ON")||
                            words.get(i+2).equals("SET QUOTED_IDENTIFIER ON")||
                            firstLine.equals("CREATE")||
                            secondLine.equals("CREATE"))
                        {
                            words.remove(i);
                            i--;
                        }
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                        System.out.println();
                    }
                    break;
                case "SET QUOTED_IDENTIFIER ON":
                    words.remove(i);
                    i--;
                    break;
                default:
                    break;
            }
        }
        return words;
    }


    /***
     * Vi havde problemer med at læse de constraints der var i create statementet, så der er lavet en metode til at
     * gøre det mere læsbart til vores program. Den sørger for at lave en newline før constraint så det bliver lettere at
     * splitte hvert statement op i linjer.
     *
     * @param listStr
     * Tager en liste af strenge som er hele create statementet for et table
     *
     * @return
     * Returnerer den samme liste som metoden fik ind, men erstatter "CONSTRAINT" med "\nCONSTRAINT".
     */
    private List<String> findCon(List<String> listStr) {
        String delimiterBefore = "CONSTRAINT";
        String delimiterAfter = "\nCONSTRAINT";
        for(int i = 0; i < listStr.size(); i++) {
            if(listStr.get(i).contains("CONSTRAINT")) {
                String a = listStr.get(i);
                a = a.replace(delimiterBefore, delimiterAfter);
                listStr.set(i, a);
            }
        }
        return listStr;
    }

    /***
     * Hovedmetoden i klassen. Formålet med denne metode er at tage strengobjekter og omforme dem til
     * table og column objekter. Det gør den meget low-tech ved at kigge på hver linje i et SQL script og dele
     * dem op derfra.
     * Metoden arbejder i 3 faser:
     * Første fase arbejder med at se hvad tablet hedder og opretter et navn som senere hen bliver smidt ned i et table objekt.
     *
     * Anden fase arbejder med at indentificere de forskellige kolonner der er. For hver kollone der er, kigger den på
     * hvilket navn den har, hvilken datatype der er og om den er nullable. Hvis det er en datatype der har length med
     * så indentificerer den også det.
     *
     * Tredje fase arbejder med at finde ud af om der er nogle constraints i strengen. I vores tilfælde er der kun en enkelt
     * constraint der beskriver primary key, så det er det den kigger efter her. Hvis vi havde haft andre constraints med i vores
     * dummydata havde vi også kigget efter det.
     *
     * Efter at den har kørt de 3 faser igennem instancierer den et table objekt den lægger ind i en liste.
     * Metoden kører lige så mange gange som der er strenge i liste parameteret
     *
     * @param path
     * Stien til hvor vi skal læse vores dataen fra. Bliver sendt videre fra metoden getTables()
     * @return
     * Returnerer en liste af tables oprettet ud fra den strengliste taget fra parameteret
     */
    private DataBaseHolder createTable(String path)
    {
        List<String> str = reader.reader(path);
        DataBaseHolder dbHolder = new DataBaseHolder();
        findCon(str);
        ArrayList<Table> arrTbl = new ArrayList<>();
        String name = null;
        int phase = 0;

        while (!str.isEmpty())
        {
            ArrayList<Column> columns = new ArrayList<>();

            //Noget af dataen vi får ind fra "str" er inddelt i "\t" istedet for "\n" som er det vi splitter statementsne op i.
            //Det fixer vi her med en replace kommando. Den kommer altid til at rette i det aktuelle table,
            //da vi sletter den foreste streng i arrayet når vi er færdige med at bruge det.
            str.set(0,str.get(0).replace("\t","\n"));
            ArrayList<String> tableLines = new ArrayList<>(Arrays.asList(str.remove(0).split("\n")));

            while (true)
            {

                if(phase == 0)
                {
                    dbHolder.setName(tableLines.get(0).substring(14, 17))   ;
                    name = tableLines.get(0).substring(20,tableLines.get(0).length()-2);
                    phase++;
                    tableLines.remove(0);
                }// phase 0 ends
                else if(phase == 1)
                {

                    while (tableLines.get(0).contains("NULL"))
                    {
                        String[] nameAndType = tableLines.get(0).split("]");
                        Column col = null;
                        while (nameAndType[0].charAt(0) != '[')
                        {
                            nameAndType[0] = nameAndType[0].substring(1,nameAndType[0].length());
                        }
                        String colName = nameAndType[0].substring(1,nameAndType[0].length());
                        String colType = nameAndType[1].substring(2,nameAndType[1].length());
                        boolean nullable = false;
                        int length = -1;
                        if(nameAndType[2].charAt(0) == '(')
                        {
                            String lengtAndNullStr = nameAndType[2].split(" ")[0];
                            length = Integer.parseInt((lengtAndNullStr.substring(1,(lengtAndNullStr.length()-1))));
                            col = new Column(colName, colType, nullable,length);
                            if(!nameAndType[2].contains("NOT NULL"))
                            {
                                nullable = true;
                            }
                        }
                        else if(!nameAndType[2].contains("NOT NULL"))
                        {
                            nullable = true;
                        }

                        if(col == null)
                        {
                            col = new Column(colName, colType, nullable);
                        }
                        columns.add(col);
                        tableLines.remove(0);
                        if(tableLines.size() == 0)
                        {
                            break;
                        }
                    }
                    phase++;
                }//phase 1 end
                else if(phase == 2)
                {
                    String constraints = "";
                    for (int i = 0; i < tableLines.size(); i++)
                    {
                        constraints += tableLines.get(i);
                    }
                    if(constraints.contains("CONSTRAINT"))
                    {
                        Pattern p = Pattern.compile("\\[(.*?)\\]");
                        Matcher m = p.matcher(constraints);
                        if (m.find())
                        {
                            m.find();
                            constraints = m.group(1).split(" ")[0];
                        }
                        for (Column c : columns)
                        {
                            if (c.getName().equals(constraints))
                            {
                                c.setPrimary(true);
                                break;
                            }
                        }
                    }
                    phase = 0;
                    Table tbl = new Table(name,columns);
                    arrTbl.add(tbl);
                    break;
                }//phase 2 end
            } //phaseloop end
        }//table loop end

        dbHolder.setTableList(arrTbl);
        return dbHolder;
    }
}
